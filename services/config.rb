
coreo_aws_rule "kms-inventory" do
  action :define
  service :kms
  link "https://kb.securestate.vmware.com/aws-all-inventory.html"
  include_violations_in_count false
  display_name "KMS Inventory"
  description "This rule performs an inventory on all kms objects in the target AWS account."
  category "Inventory"
  suggested_action "None."
  level "Informational"
  objectives ["keys"]
  audit_objects ["object.keys.key_id"]
  operators ["=~"]
  raise_when [//]
  id_map "object.keys.key_id"
end

coreo_aws_rule "kms-unused" do
  action(${AUDIT_AWS_KMS_ALERT_LIST}.include?("kms-unused") ? :define : :nothing)
  service :user
  link "https://kb.securestate.vmware.com/aws-all-inventory.html"
  include_violations_in_count false
  display_name "KMS is unused"
  description "This rule performs an inventory on all kms objects to determine if KMS is being used as a cryptographic management system. If not, it indicated a system may not be in use."
  category "Security"
  suggested_action "None."
  level "Low"
  meta_nist_171_id "3.13.10"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [""]
  id_map "static.no_op"
  meta_rule_empty_violation true
  meta_compliance (
    [
      { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.13.10" }
    ]
  )
  meta_rule_query <<~QUERY
  { 
    query(func: <%= filter['key'] %>) {
        <%= default_predicates %>
    }
  }
  QUERY
  meta_rule_node_triggers ({
          'key' => []
  })
end

coreo_aws_rule "kms-key-rotates" do
  action :define
  service :kms
  link "https://kb.securestate.vmware.com/aws-kms-key-rotates.html"
  include_violations_in_count true
  display_name "Verify rotation for customer created CMKs is enabled"
  description "AWS Key Management Service (KMS) allows customers to rotate the backing key which is key material stored within the KMS which is tied to the key ID of the Customer Created customer master key (CMK). It is the backing key that is used to perform cryptographic operations such as encryption and decryption. Automated key rotation currently retains all prior backing keys so that decryption of encrypted data can take place transparently."
  category "Audit"
  suggested_action "It is recommended that CMK key rotation be enabled."
  level "Medium"
  meta_cis_id "2.8"
  meta_cis_scored "true"
  meta_cis_level "2"
  objectives ["keys", "key_rotation_status"]
  call_modifiers [{}, {:key_id => "object.keys.key_id"}]
  audit_objects ["", "object.key_rotation_enabled"]
  operators ["", "=="]
  raise_when ["", false]
  id_map "modifiers.key_id"
  meta_compliance (
    [
      { "name" => "cis-aws-foundations-benchmark", "version" => "1.2.0", "requirement" => "2.8" }
    ]
  )
  meta_rule_query <<~QUERY
  { 
    keys as var(func: <%= filter['key'] %>) @cascade {         
      keys_rotation as relates_to @filter(has(key_rotation_status)) {
        kre as key_rotation_enabled
      }
    }
    query(func: uid(keys)) @cascade {
      <%= default_predicates %>
      relates_to @filter(uid(keys_rotation) AND eq(val(kre), false) ) {
        <%= default_predicates %>
      }
    }    
  }
  QUERY
  meta_rule_node_triggers ({
          'key' => [],
          'key_rotation_status' => ['key_rotation_enabled']
  })
end

coreo_uni_util_variables "kms-planwide" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.kms-planwide.composite_name' => 'PLAN::stack_name'},
                {'COMPOSITE::coreo_uni_util_variables.kms-planwide.plan_name' => 'PLAN::name'},
                {'COMPOSITE::coreo_uni_util_variables.kms-planwide.results' => 'unset'},
                {'GLOBAL::number_violations' => '0'}
            ])
end

coreo_aws_rule_runner "advise-kms" do
  action :run
  rules ${AUDIT_AWS_KMS_ALERT_LIST}
  rules ${AUDIT_AWS_KMS_ALERT_LIST}.push('kms-inventory').uniq if ${AUDIT_AWS_KMS_ALERT_LIST}.include?("kms-unused")
  service :kms
  regions ${AUDIT_AWS_KMS_REGIONS}
  filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end

coreo_uni_util_variables "kms-update-planwide-1" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.kms-planwide.results' => 'COMPOSITE::coreo_aws_rule_runner.advise-kms.report'},
                {'GLOBAL::number_violations' => 'COMPOSITE::coreo_aws_rule_runner.advise-kms.number_violations'},

            ])
end

coreo_uni_util_jsrunner "advise-kms-used" do
  action (${AUDIT_AWS_KMS_ALERT_LIST}.include?("kms-unused") ? :run : :nothing)
  json_input 'COMPOSITE::coreo_aws_rule_runner.advise-kms.report'
  data_type "json"
  function <<-RUBY

    var kmsIsUsed = false;
    for (var x = 0; x < Object.keys(json_input).length; x++) {
        var region = Object.keys(json_input)[x];
        const kmsKeys = Object.keys(json_input[region]);

        if (kmsKeys.length > 0) {
            kmsIsUsed = true;
        }
        if (kmsIsUsed === true) {
            break;
        }
    }
    var ret = json_input;
    if (kmsIsUsed === false) {
        ret = addViolation(json_input, 'kms-unused');
    }
    coreoExport('report', JSON.stringify(ret));
    callback(ret);

}

const ruleMetaJSON = {
     'kms-unused': COMPOSITE::coreo_aws_rule.kms-unused.inputs
 };

const ruleInputsToKeep = ['service', 'category', 'link', 'display_name', 'suggested_action', 'description', 'level', 'meta_cis_id', 'meta_cis_scored', 'meta_cis_level', 'include_violations_in_count', 'meta_nist_171_id'];
const ruleMeta = {};

Object.keys(ruleMetaJSON).forEach(rule => {
    const flattenedRule = {};
    ruleMetaJSON[rule].forEach(input => {
        if (ruleInputsToKeep.includes(input.name))
            flattenedRule[input.name] = input.value;
    })
    flattenedRule["service"] = "kms";
    ruleMeta[rule] = flattenedRule;
});

function addViolation(json_input, rule_name) {
    if(${AUDIT_AWS_KMS_ALERT_LIST}.indexOf(rule_name) == -1) { return json_input; }
    if (!json_input) {
        json_input = {};
    }
    if (!json_input['us-east-1']) {
        json_input['us-east-1'] = {};
    }
    if (!json_input['us-east-1']['customer']) {
        json_input['us-east-1']['customer'] = {};
    }
    if (!json_input['us-east-1']['customer']['violating_info']) {
        json_input['us-east-1']['customer']['violating_info'] = {};
    }
    if (!json_input['us-east-1']['customer']['violations']) {
        json_input['us-east-1']['customer']['violations'] = {}
    }
    json_input['us-east-1']['customer']['violator_info'] = { 'name': 'customer' };
    json_input['us-east-1']['customer']['violations'][rule_name] = Object.assign(ruleMeta[rule_name]);

    return json_input;

RUBY
end

coreo_uni_util_variables "kms-update-for-kms-unused" do
  action (${AUDIT_AWS_KMS_ALERT_LIST}.include?("kms-unused") ? :set : :nothing)
  variables([
                {'COMPOSITE::coreo_uni_util_variables.kms-planwide.results' => 'COMPOSITE::coreo_uni_util_jsrunner.advise-kms-used.report'},
                {'COMPOSITE::coreo_aws_rule_runner.advise-kms.report' => 'COMPOSITE::coreo_uni_util_jsrunner.advise-kms-used.report'}
            ])
end

coreo_uni_util_jsrunner "tags-to-notifiers-array-kms" do
  action :run
  data_type "json"
  provide_composite_access true
  packages([
               {
                   :name => "cloudcoreo-jsrunner-commons",
                   :version => "1.10.7-beta65"
               },
               {
                   :name => "js-yaml",
                   :version => "3.7.0"
               }
           ])
  json_input '{ "compositeName":"PLAN::stack_name",
                "planName":"PLAN::name",
                "teamName":"PLAN::team_name",
                "cloudAccountName": "PLAN::cloud_account_name",
                "violations": COMPOSITE::coreo_aws_rule_runner.advise-kms.report}'
  function <<-EOH
const compositeName = json_input.compositeName;
const planName = json_input.planName;
const cloudAccount = json_input.cloudAccountName;
const cloudObjects = json_input.violations;
const teamName = json_input.teamName;

const NO_OWNER_EMAIL = "${AUDIT_AWS_KMS_ALERT_RECIPIENT}";
const OWNER_TAG = "${AUDIT_AWS_KMS_OWNER_TAG}";
const ALLOW_EMPTY = "${AUDIT_AWS_KMS_ALLOW_EMPTY}";
const SEND_ON = "${AUDIT_AWS_KMS_SEND_ON}";
const htmlReportSubject = "${HTML_REPORT_SUBJECT}";

const alertListArray = ${AUDIT_AWS_KMS_ALERT_LIST};
const ruleInputs = {};

let userSuppression;
let userSchemes;

const fs = require('fs');
const yaml = require('js-yaml');
function setSuppression() {
  try {
      userSuppression = yaml.safeLoad(fs.readFileSync('./suppression.yaml', 'utf8'));
  } catch (e) {
    if (e.name==="YAMLException") {
      throw new Error("Syntax error in suppression.yaml file. "+ e.message);
    }
    else{
      console.log(e.name);
      console.log(e.message);
      userSuppression=[];
    }
  }

  coreoExport('suppression', JSON.stringify(userSuppression));
}

function setTable() {
  try {
    userSchemes = yaml.safeLoad(fs.readFileSync('./table.yaml', 'utf8'));
  } catch (e) {
    if (e.name==="YAMLException") {
      throw new Error("Syntax error in table.yaml file. "+ e.message);
    }
    else{
      console.log(e.name);
      console.log(e.message);
      userSchemes={};
    }
  }

  coreoExport('table', JSON.stringify(userSchemes));
}
setSuppression();
setTable();

const argForConfig = {
    NO_OWNER_EMAIL, cloudObjects, userSuppression, OWNER_TAG,
    userSchemes, alertListArray, ruleInputs, ALLOW_EMPTY,
    SEND_ON, cloudAccount, compositeName, planName, htmlReportSubject, teamName
}


function createConfig(argForConfig) {
    let JSON_INPUT = {
        compositeName: argForConfig.compositeName,
        htmlReportSubject: argForConfig.htmlReportSubject,
        planName: argForConfig.planName,
        teamName: argForConfig.teamName,
        violations: argForConfig.cloudObjects,
        userSchemes: argForConfig.userSchemes,
        userSuppression: argForConfig.userSuppression,
        alertList: argForConfig.alertListArray,
        disabled: argForConfig.ruleInputs,
        cloudAccount: argForConfig.cloudAccount
    };
    let SETTINGS = {
        NO_OWNER_EMAIL: argForConfig.NO_OWNER_EMAIL,
        OWNER_TAG: argForConfig.OWNER_TAG,
        ALLOW_EMPTY: argForConfig.ALLOW_EMPTY, SEND_ON: argForConfig.SEND_ON,
        SHOWN_NOT_SORTED_VIOLATIONS_COUNTER: false
    };
    return {JSON_INPUT, SETTINGS};
}

const {JSON_INPUT, SETTINGS} = createConfig(argForConfig);
const CloudCoreoJSRunner = require('cloudcoreo-jsrunner-commons');

const emails = CloudCoreoJSRunner.createEmails(JSON_INPUT, SETTINGS);
const suppressionJSON = CloudCoreoJSRunner.createJSONWithSuppress(JSON_INPUT, SETTINGS);

coreoExport('JSONReport', JSON.stringify(suppressionJSON));
coreoExport('report', JSON.stringify(suppressionJSON['violations']));

callback(emails);
  EOH
end

coreo_uni_util_variables "kms-update-planwide-3" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.kms-planwide.results' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kms.JSONReport'},
                {'COMPOSITE::coreo_aws_rule_runner.advise-kms.report' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kms.report'},
                {'GLOBAL::table' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kms.table'}
            ])
end

coreo_uni_util_jsrunner "tags-rollup-kms" do
  action :run
  data_type "text"
  json_input 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kms.return'
  function <<-EOH
const notifiers = json_input;

function setTextRollup() {
    let emailText = '';
    let numberOfViolations = 0;
    let usedEmails=new Map();
    notifiers.forEach(notifier => {
        const hasEmail = notifier['endpoint']['to'].length;
        const email = notifier['endpoint']['to'];
        if(hasEmail && usedEmails.get(email)!==true) {
            usedEmails.set(email,true);
            numberOfViolations += parseInt(notifier['num_violations']);
            emailText += "recipient: " + notifier['endpoint']['to'] + " - " + "Violations: " + notifier['numberOfViolatingCloudObjects'] + ", Cloud Objects: "+ (notifier["num_violations"]-notifier['numberOfViolatingCloudObjects']) + "\\n";
        }
    });

    textRollup += 'Total Number of matching Cloud Objects: ' + numberOfViolations + "\\n";
    textRollup += 'Rollup' + "\\n";
    textRollup += emailText;

}



let textRollup = '';
setTextRollup();

callback(textRollup);
  EOH
end

coreo_uni_util_notify "advise-kms-to-tag-values" do
  action((("${AUDIT_AWS_KMS_ALERT_RECIPIENT}".length > 0)) ? :notify : :nothing)
  notifiers 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kms.return'
end

coreo_uni_util_notify "advise-kms-rollup" do
  action((("${AUDIT_AWS_KMS_ALERT_RECIPIENT}".length > 0) and (! "${AUDIT_AWS_KMS_OWNER_TAG}".eql?("NOT_A_TAG"))) ? :notify : :nothing)
  type 'email'
  allow_empty ${AUDIT_AWS_KMS_ALLOW_EMPTY}
  send_on '${AUDIT_AWS_KMS_SEND_ON}'
  payload '
composite name: PLAN::stack_name
plan name: PLAN::name
COMPOSITE::coreo_uni_util_jsrunner.tags-rollup-kms.return
  '
  payload_type 'text'
  endpoint ({
      :to => '${AUDIT_AWS_KMS_ALERT_RECIPIENT}', :subject => 'CloudCoreo kms rule results on PLAN::stack_name :: PLAN::name'
  })
end

coreo_aws_s3_policy "cloudcoreo-audit-aws-kms-policy" do
  action((("${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :create : :nothing)
  policy_document <<-EOF
{
"Version": "2012-10-17",
"Statement": [
{
"Sid": "",
"Effect": "Allow",
"Principal":
{ "AWS": "*" }
,
"Action": "s3:*",
"Resource": [
"arn:aws:s3:::bucket-${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}/*",
"arn:aws:s3:::bucket-${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}"
]
}
]
}
  EOF
end

coreo_aws_s3_bucket "bucket-${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}" do
  action((("${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :create : :nothing)
  bucket_policies ["cloudcoreo-audit-aws-kms-policy"]
end

coreo_uni_util_notify "cloudcoreo-audit-aws-kms-s3" do
  action((("${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :notify : :nothing)
  type 's3'
  allow_empty true
  payload 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kms.report'
  endpoint ({
      object_name: 'aws-kms-json',
      bucket_name: 'bucket-${AUDIT_AWS_KMS_S3_NOTIFICATION_BUCKET_NAME}',
      folder: 'kms/PLAN::name',
      properties: {}
  })
end
